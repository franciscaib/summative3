SELECT Department.department_id, Department.department_name, YEAR(Employee.hire_date), count(Employee.employee_id) as "Jumlah Karyawan"
FROM Department
JOIN Employee ON Department.department_id = Employee.department_id
GROUP BY Department.department_id;