CREATE DATABASE Summativedb;

USE Summativedb;

CREATE TABLE Student(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    birthdate DATE NOT NULL,
    gender VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)  
);

CREATE TABLE Lesson(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    level INT NOT NULL,
    PRIMARY KEY (id)  
);

CREATE TABLE Score(
    id INT NOT NULL AUTO_INCREMENT,
    student_id INT NOT NULL,
    lesson_id INT NOT NULL,
    score INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_student_id 
	FOREIGN KEY(student_id) REFERENCES Student(id),
    CONSTRAINT fk_lesson_id 
	FOREIGN KEY(lesson_id) REFERENCES Lesson(id)
    ON DELETE CASCADE  
    ON UPDATE CASCADE
);

INSERT INTO Student(name, surname, birthdate, gender) VALUES
('Francisca Intan', 'Bertin', '1999-12-03', 'Perempuan'),
('Yohanes Arthur', 'Pahlevi', '2008-10-28', 'Laki - laki'),
('Margaretha Nayang', 'Christanti', '2004-10-18', 'Perempuan');

INSERT INTO Lesson(name, level) VALUES
('Algoritma', 1),
('Struktur Data', 3),
('Simulasi Jaringan', 7),
('PBO', 3),
('DTN', 6); 

INSERT INTO Score(student_id, lesson_id,score) VALUES
(1, 1, 90),
(2, 1, 70),
(3, 2, 100),
(1, 4, 60),
(3, 5, 45),
(2, 2, 79),
(1, 5, 65); 