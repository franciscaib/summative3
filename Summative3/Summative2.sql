SELECT concat(student.name, ' ',student.surname)  as "Full Name", student.birthdate, student.gender, lesson.name, score.score
FROM Score
JOIN Lesson ON score.lesson_id = lesson.id
JOIN Student ON score.student_id = Student.id;